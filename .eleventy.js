const eleventyNavigationPlugin = require("@11ty/eleventy-navigation");

module.exports = function(eleventyConfig) {
  eleventyConfig.addPlugin(eleventyNavigationPlugin);
  eleventyConfig.addPassthroughCopy("assets");

  // Get only content that matches a tag - https://www.11ty.dev/docs/collections/ (bottom half of page)
  eleventyConfig.addCollection("posts", function(collection) {
    return collection.getFilteredByTag("posts");
  });

  eleventyConfig.addCollection("flowers", function(collection) {
    return collection.getFilteredByTag("flower");
  });
  
  return {
    passthroughFileCopy: true,
    markdownTemplateEngine: "njk",
    templateFormats: ["html", "njk", "md"],
    dir: {
      input: "src",
      output: "_site",
      includes: "includes"
    }
  }
}