---
title: 'Marguerit'
permalink: "/flower/{{ title | slugify }}/"
imageSrc: "margo.jpg"
bloomTime: "spring"
date: 2020-01-01
eleventyExcludeFromCollections: false
---


![Marguerit](/assets/images/margo.jpg)

This is a Marguerit