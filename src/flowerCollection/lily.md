---
title: 'Lily'
permalink: "/flower/{{ title | slugify }}/"
imageSrc: "lily.jpeg"
bloomTime: "summer"
date: 2020-01-01
eleventyExcludeFromCollections: false
---


![Lily](/assets/images/lily.jpg)

This is a Lily