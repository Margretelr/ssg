let myCanvas;
let angle = 0;
let num = 13;
let size = 50;
function setup() {
    console.log("cursor.js is loaded");
    myCanvas = createCanvas(windowWidth, windowHeight);
    myCanvas.parent("p5Wrapper");
    colorMode(RGB);
    angleMode(DEGREES);
    
}

function draw() {
    clear();
    
    fill(56, 74, 211, 50);
    noStroke();
    ellipse(mouseX, mouseY, size);

    push();
    translate(mouseX, mouseY);
    fill(248, 215, 28, 150);
    rotate(angle);
    angle+=0.5;
    for(let i= 0; i<num; i++){
        let cir = 360/num;
        rotate(cir);
        ellipse(0, size, 20, size);
    }
    pop();
}